package ua.epam.spring.hometask;
import java.sql.SQLException;

@FunctionalInterface
public interface Printable {
    void print() throws SQLException;
}
