package ua.epam.spring.hometask;

import com.sun.org.apache.bcel.internal.generic.NEW;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Controller {

    Controller(){}

    private User anton;
    private User andrew;
    private Event lalalend;
    private Event shrek;
    private Event matrix;
    private Ticket andrewsTicket;
    private Ticket antonsTicket;
    private UserServiceImpl userServiceImpl;
    private EventServiceImpl eventServiceImpl;
    private AuditoriumServiceImpl auditoriumServiceImpl;
    private BookingServiceImpl bookingServiceImpl;
    private Map<Integer, String> menu = new HashMap<>();
    private Map<String, Printable> methodsMenu = new HashMap<>();
    private Scanner scan = new Scanner(System.in, "UTF-8");

    public void setAndrew(User andrew) {
        this.andrew = andrew;
    }

    public void setAnton(User anton) {
        this.anton = anton;
    }

    public void setAndrewsTicket(Ticket andrewsTicket) {
        this.andrewsTicket = andrewsTicket;
    }

    public void setAntonsTicket(Ticket antonsTicket) {
        this.antonsTicket = antonsTicket;
    }

    public void setBookingServiceImpl(BookingServiceImpl bookingServiceImpl) {
        this.bookingServiceImpl = bookingServiceImpl;
    }

    public void setEventServiceImpl(EventServiceImpl eventServiceImpl) {
        this.eventServiceImpl = eventServiceImpl;
    }

    public void setLalalend(Event lalalend) {
        this.lalalend = lalalend;
    }

    public void setShrek(Event shrek) {
        this.shrek = shrek;
    }

    public void setMatrix(Event matrix) {
        this.matrix = matrix;
    }

    public void setAuditoriumServiceImpl(AuditoriumServiceImpl auditoriumServiceImpl) {
        this.auditoriumServiceImpl = auditoriumServiceImpl;
    }

    public void setUserServiceImpl(UserServiceImpl userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    void init(){
        menu.put(0, "show event by name or id\n");
        menu.put(1, "show user by mail\n");
        menu.put(2, "show all users\n");
        menu.put(3, "show all auditoriums\n");
        menu.put(4, "show auditorium by name\n");
        menu.put(5, "show purchased tickets\n");
        menu.put(6, "Exit. \n");
        methodsMenu.put("0", this::showEvent);
        methodsMenu.put("1", this::showUserByEmail);
        methodsMenu.put("2", this::showAllUsers);
        methodsMenu.put("3", this::showAllAuditoriums);
        methodsMenu.put("4", this::showAuditoriumByName);
        methodsMenu.put("5", this::showPurchasedTickets);
        methodsMenu.put("6", this::quit);
        userServiceImpl.save(andrew);
        userServiceImpl.save(anton);
        eventServiceImpl.save(lalalend);
        eventServiceImpl.save(shrek);
        eventServiceImpl.save(matrix);
        bookingServiceImpl.bookTickets(Stream.of(andrewsTicket, antonsTicket).collect(Collectors.toSet()));
    }

    void show() {
        String keyMenu;
        do {
            showInfo();
            System.out.println("Please, select menu point.\n");
            keyMenu = scan.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("1"));
    }

    private void quit() {
        System.out.println("Exit\n");}

    private void showInfo() {
        System.out.println("Menu for TextTask\n");
        menu.forEach((key, elem) -> System.out.println(key + " : " + elem));
    }

    private void showEvent(){
        System.out.println("1 for name | 2 for id ");
        int identifier = scan.nextInt();
        if(identifier == 1){
            System.out.println("name:");
            String name = scan.next();
            System.out.println(eventServiceImpl.getByName(name));
         }
        else
            if(identifier == 2){
                System.out.println("id:");
                int id = scan.nextInt();
                System.out.println(eventServiceImpl.getById(id));
            }
    }

    private void showUserByEmail(){
        System.out.println("input email");
        String email = scan.nextLine();
        System.out.println(userServiceImpl.getUserByEmail(email));
    }

    private void showAllUsers(){
        userServiceImpl.printAll();
    }

    private void showAllAuditoriums(){
        auditoriumServiceImpl.printAll();
    }

    private void showAuditoriumByName(){
        System.out.println("input name");
        String name = scan.next();
        System.out.println(auditoriumServiceImpl.getByName(name));
    }

    private void showPurchasedTickets(){
    eventServiceImpl.getAll().forEach(event ->
            event.getTickets().forEach(ticket -> System.out.println(ticket.getSeat())));
    }
}
