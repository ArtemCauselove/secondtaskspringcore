package ua.epam.spring.hometask.service;

import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;

import java.time.LocalDateTime;
import java.util.List;

public class DiscountService {

    private List<DiscountStrategy> discountStrategies;

    public double getDiscount(User user, Event event,
                       LocalDateTime dateTime, int numberOfTickets) {
        return executeDiscount(user, dateTime, numberOfTickets);
    }

    public double executeDiscount(User user,
                                   LocalDateTime dateTime, int numberOfTickets){
        double bestDiscount = 1;
        for(DiscountStrategy discountType : discountStrategies){
            double currentDiscount = discountType.calculateDiscount(numberOfTickets, user, dateTime);
             if(currentDiscount < bestDiscount){
                 bestDiscount = currentDiscount;
             }
        }
        return bestDiscount;
    }

    public void setDiscountStrategies(List<DiscountStrategy> discountStrategies) {
        this.discountStrategies = discountStrategies;
    }

}
