package ua.epam.spring.hometask.service;

import ua.epam.spring.hometask.domain.User;

import java.time.LocalDateTime;

public class SuperStrategy implements DiscountStrategy {

    double discountPercent;

    @Override
    public double calculateDiscount(int numberOfTickets, User user, LocalDateTime dateTime) {
        if (user.getDateOfBirth().getDayOfMonth() - dateTime.getDayOfMonth() < 5) {
            if (numberOfTickets > 9){
                return discountPercent;
            }
        }
        return 1;
    }

    public void setDiscountPercent(double discount) {
        this.discountPercent = discount;
    }
}
