package ua.epam.spring.hometask.service;

import ua.epam.spring.hometask.domain.User;

import java.time.LocalDateTime;

public interface DiscountStrategy {
    public double calculateDiscount(int numberOfTickets, User user, LocalDateTime dateTime);
    
}
