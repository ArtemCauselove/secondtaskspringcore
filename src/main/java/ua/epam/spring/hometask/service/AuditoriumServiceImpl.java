package ua.epam.spring.hometask.service;

import org.jetbrains.annotations.NotNull;
import ua.epam.spring.hometask.domain.Auditorium;

import java.util.Objects;
import java.util.Set;

public class AuditoriumServiceImpl implements AuditoriumService {

    private Set<Auditorium> setOfAuditories;

    public AuditoriumServiceImpl() { }

    public AuditoriumServiceImpl(Set<Auditorium> setOfAuditories) {
        this.setOfAuditories = setOfAuditories;
    }

    public Set<Auditorium> getSetOfAuditories() {
        return setOfAuditories;
    }

    public void setSetOfAuditories(Set<Auditorium> setOfAuditories) {
        this.setOfAuditories = setOfAuditories;
    }

    @NotNull
    public Set<Auditorium> getAll(){
        return setOfAuditories;
    }

    public void printAll(){
        getAll().forEach(c-> System.out.println( c.getName() + " " +c.getNumberOfSeats()+ " vips: " + c.getVipSeats()));
    }

    public Auditorium getByName(@NotNull String name){
        return setOfAuditories.stream().filter(data -> Objects.equals(data.getName(), name)).findFirst().get();
    }

}
