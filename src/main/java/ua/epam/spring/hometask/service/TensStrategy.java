package ua.epam.spring.hometask.service;

import ua.epam.spring.hometask.domain.User;

import java.time.LocalDateTime;

public class TensStrategy implements DiscountStrategy {
    @Override
    public double calculateDiscount(int numberOfTickets, User user, LocalDateTime dateTime) {
        if (numberOfTickets > 9) {
            double numberOfFreeTickets =  Math.floor(numberOfTickets / 10);
            return (numberOfTickets - numberOfFreeTickets) / numberOfTickets;
        }
        else return 1;
    }
}
