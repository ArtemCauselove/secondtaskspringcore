package ua.epam.spring.hometask.service;

import ua.epam.spring.hometask.domain.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserServiceImpl implements UserService {
    private List<User> users = new ArrayList<>();
    @Nullable
    @Override
    public User getUserByEmail(@Nonnull String email) {
        return users.stream().filter((user) -> user.getEmail().equals(email)).findAny().get();
    }

    @Override
    public User save(@Nonnull User object) {
        users.add(object);
        return object;
    }

    @Override
    public void remove(@Nonnull User object) {
        users.remove(object);
    }

    @Override
    public User getById(int id) {
        return users.stream().filter((user) -> user.getId().equals(id)).findAny().get();
    }

    @Nonnull
    @Override
    public Collection<User> getAll() {
        return users;
    }

    public void printAll(){
        getAll().forEach(c-> System.out.println( c.getFirstName() + " " +c.getLastName()));
    }
}
