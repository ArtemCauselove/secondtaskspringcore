package ua.epam.spring.hometask.service;

import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.EventRating;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;

import javax.annotation.Nonnull;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class BookingServiceImpl implements BookingService {

    private DiscountService discount;

    @Override
    public double getTicketsPrice(@Nonnull Event event, @Nonnull LocalDateTime dateTime, User user, @Nonnull Set<Long> seats) {
        double totalPrice = 0;
        for(Long currentSeat: seats){
            if(event.getAuditoriums().get(dateTime).getVipSeats().contains(currentSeat)){
                totalPrice = totalPrice + event.getBasePrice()*(2);
            }
            else
                totalPrice = totalPrice + event.getBasePrice();
        }
        if(event.getRating()== EventRating.HIGH){
            totalPrice = totalPrice * 1.15;
        }

        return totalPrice * discount.getDiscount(user, event, dateTime, seats.size());
    }

    @Override
    public void bookTickets(@Nonnull Set<Ticket> tickets) {
        for(Ticket currentTicket : tickets){
            currentTicket.getEvent().setTicket(currentTicket);
        }
    }

    @Nonnull
    @Override
    public Set<Ticket> getPurchasedTicketsForEvent(@Nonnull Event event, @Nonnull LocalDateTime dateTime) {
        return event.getTickets().stream().filter(ticket -> Objects.equals(ticket.getDateTime(), dateTime)).collect(Collectors.toSet());
    }

    public void setDiscount(DiscountService discount) {
        this.discount = discount;
    }
}
