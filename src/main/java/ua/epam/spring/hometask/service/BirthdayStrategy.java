package ua.epam.spring.hometask.service;

import ua.epam.spring.hometask.domain.User;

import java.time.LocalDateTime;

public class BirthdayStrategy implements DiscountStrategy {

    private double discountPercent;

    public void setDiscountPercent(double discountPercent) {
        this.discountPercent = discountPercent;
    }

    @Override
    public double calculateDiscount(int numberOfTickets, User user, LocalDateTime dateTime) {
        if (user.getDateOfBirth().getDayOfMonth() - dateTime.getDayOfMonth() < 5) {
        return discountPercent; }
        else return 1;
    }
}
