package ua.epam.spring.hometask.service;


import ua.epam.spring.hometask.domain.Event;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.*;

public class EventServiceImpl implements EventService {
    List<Event> events = new ArrayList<>();
    @Nullable
    @Override
    public Event getByName(@Nonnull String name) {
        return events.stream().
                filter(event -> name.equals(event.getName())).
                findAny().orElse(null);
    }

    @Override
    public Event save(@Nonnull Event object) {
        events.add(object);
        return object;
    }

    @Override
    public void remove(@Nonnull Event object) {
        events.remove(object);
    }

    @Override
    public Event getById(@Nonnull int id) {
        return events.get(id);
    }

    @Nonnull
    @Override
    public Collection<Event> getAll() {
        return events;
    }
}
