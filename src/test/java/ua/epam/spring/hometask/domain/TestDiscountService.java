package ua.epam.spring.hometask.domain;

import org.junit.Before;
import org.junit.Test;
import ua.epam.spring.hometask.service.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class TestDiscountService {
    private User user1;
    private Event event;
    private DiscountService discountService;
    private SuperStrategy superStrategy;
    private TensStrategy tensStrategy;
    private BirthdayStrategy birthdayStrategy;
    private LocalDate userBirth;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    private LocalDateTime dt = LocalDateTime.parse("2016-01-12 10:30", formatter);
    @Before
    public void setUp(){
        user1 = new User();
        event = new Event();
        discountService = new DiscountService();
        superStrategy = new SuperStrategy();
        birthdayStrategy = new BirthdayStrategy();
        tensStrategy = new TensStrategy();
        superStrategy.setDiscountPercent(0.7);
        birthdayStrategy.setDiscountPercent(0.95);
        discountService.setDiscountStrategies(
                Arrays.asList(superStrategy, tensStrategy, birthdayStrategy));
        userBirth = LocalDate.of(1999, Month.JANUARY, 20);
    }
    @Test
    public void testExecutingTenthDiscount(){
        event.setBasePrice(100);
        user1.setDateOfBirth(userBirth);
        assertEquals(0.9, discountService.executeDiscount(user1, dt,10), 0.01);
        assertEquals(0.93, discountService.executeDiscount(user1, dt,15), 0.01);
        assertEquals(0.9, discountService.executeDiscount(user1, dt,20), 0.01);
    }

    @Test
    public void testExecutingSuperDiscount(){
        user1.setDateOfBirth(LocalDate.of(1999, Month.JANUARY, 15));
        double discount = discountService.executeDiscount(user1, dt,20);
        assertEquals(0.7, discount, 0.001);
    }

}
