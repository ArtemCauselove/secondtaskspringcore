package ua.epam.spring.hometask.domain;

import org.junit.Before;
import org.junit.Test;
import ua.epam.spring.hometask.service.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class TestBookingService {
    private User user1;
    private Event event;
    private DiscountService discountService;
    private BookingServiceImpl bookingServiceImpl;
    private SuperStrategy superStrategy;
    private TensStrategy tensStrategy;
    private BirthdayStrategy birthdayStrategy;
    private LocalDate userBirth;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    private LocalDateTime dt = LocalDateTime.parse("2016-01-12 10:30", formatter);
    @Before
    public void setUp(){
        user1 = new User();
        event = new Event();
        discountService = new DiscountService();
        superStrategy = new SuperStrategy();
        birthdayStrategy = new BirthdayStrategy();
        tensStrategy = new TensStrategy();
        bookingServiceImpl = new BookingServiceImpl();
        superStrategy.setDiscountPercent(0.7);
        birthdayStrategy.setDiscountPercent(0.95);
        discountService.setDiscountStrategies(
                Arrays.asList(superStrategy, tensStrategy, birthdayStrategy));
        userBirth = LocalDate.of(1999, Month.JANUARY, 15);
    }
    @Test
    public void testGettingPrice(){
        Auditorium aud = new Auditorium();
        aud.setVipSeats(Stream.of(1L, 2L).collect(Collectors.toSet()));
        event.setAuditorium(dt, aud);
        event.setBasePrice(100);
        event.setRating(EventRating.HIGH);
        user1.setDateOfBirth(userBirth);
        bookingServiceImpl.setDiscount(discountService);
        /*
         * price = (3*100 + 200*2)*1.25 = 800
         * total price = price * discount(birthday) = 763.8
         * */
        double price = bookingServiceImpl.getTicketsPrice(event, dt, user1, Stream.of(1L, 2L, 3L, 4L, 5L).collect(Collectors.toSet()) );
        assertEquals(764, price, 1);
    }
}
